<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'currency', 'amount', 'account_number','provider_id'];

    public function providers()
    {
        return $this->belongsTo(Provider::class);
    }
}
